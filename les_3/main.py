from abc import ABCMeta, abstractmethod
from configparser import ConfigParser
import error


class Utils:
    @staticmethod
    def get_conf_params(chapter_name: str, param_name: str):
        config = ConfigParser()
        config.read("../settings.conf")
        return config[chapter_name][param_name]

    @staticmethod
    def list_str_to_int(params: list):
        for i, item in enumerate(params):
            params[i] = int(item)
        return params


class Vector:
    def __init__(self, vector: list) -> None:
        self.vector = vector

    def __add__(self: 'Vector', v2: 'Vector') -> 'Vector':
        new_vector = list()
        i = 0
        while i <= self.vector.__len__() - 1:
            new_vector.append(self.vector[i] + v2.vector[i])
            i += 1
        return Vector(new_vector)

    def __mod__(self: 'Vector', v2: 'Vector') -> 'Vector':
        new_vector = list()
        i = 0
        while i <= self.vector.__len__() - 1:
            new_vector.append(self.vector[i] % v2.vector[i])
            i += 1
        return Vector(new_vector)


# interface
class UObject(metaclass=ABCMeta):
    @abstractmethod
    def get_property(self, key: str):
        pass

    @abstractmethod
    def set_property(self, key: str, value: object) -> None:
        pass


# interface
class IMovable(metaclass=ABCMeta):
    @abstractmethod
    def get_position(self) -> Vector:
        pass

    @abstractmethod
    def set_position(self, new_value: Vector) -> None:
        pass

    @abstractmethod
    def get_velocity(self) -> Vector:
        pass


# interface
class IRotable(metaclass=ABCMeta):
    @abstractmethod
    def get_direction(self) -> Vector:
        pass

    @abstractmethod
    def set_direction(self, new_value: Vector) -> None:
        pass

    @abstractmethod
    def get_angular_velocity(self) -> Vector:
        pass

    @abstractmethod
    def set_angular_velocity(self, new_value: Vector) -> None:
        pass


# interface
class Command(metaclass=ABCMeta):
    @abstractmethod
    def execute(self) -> None:
        pass


class MovableAdapter(IMovable):
    def __init__(self, obj: UObject) -> None:
        self._obj = obj

    def get_position(self) -> Vector:
        return Vector(self._obj.get_property("position"))

    def set_position(self, new_value: Vector) -> None:
        self._obj.set_property("position", new_value)

    def get_velocity(self) -> Vector:
        return Vector(self._obj.get_property("velocity"))


class RotableAdapter(IRotable):
    def __init__(self, obj: UObject) -> None:
        self._obj = obj

    def get_direction(self) -> Vector:
        return Vector(self._obj.get_property("direction"))

    def set_direction(self, new_value: Vector) -> None:
        self._obj.set_property("direction", new_value)

    def get_angular_velocity(self) -> Vector:
        return Vector(self._obj.get_property("angular_velocity"))

    def set_angular_velocity(self, new_value: Vector) -> None:
        self._obj.set_property("angular_velocity", new_value)


class Move(Command):
    def __init__(self, movable: IMovable) -> None:
        self.__movable = movable

    def execute(self) -> None:
        try:
            _position = self.__movable.get_position()
            _velocity = self.__movable.get_velocity()
        except AttributeError:
            raise error.NotReadPropertice("not read propertice")

        _max_position = Vector(
            Utils.list_str_to_int(list(Utils.get_conf_params('SETTINGS', 'max_position').split(','))))
        _min_position = Vector(
            Utils.list_str_to_int(list(Utils.get_conf_params('SETTINGS', 'min_position').split(','))))
        _position_new = _position + _velocity

        if _position_new.vector < _min_position.vector or _position_new.vector > _max_position.vector:
            raise error.OutOfRange("not move object")
        else:
            self.__movable.set_position(_position_new)


class Rotate(Command):
    def __init__(self, rotable: IRotable) -> None:
        self.__rotable = rotable

    def execute(self) -> None:
        try:
            _direction = self.__rotable.get_direction()
            _angular_velocity = self.__rotable.get_angular_velocity()
        except AttributeError:
            raise error.NotReadPropertice("not read propertice")

        max_direction = list()
        max_direction.append(int(Utils.get_conf_params("DEFAULT", "max_direction")))
        max_direct = Vector(max_direction)

        self.__rotable.set_direction((_direction + _angular_velocity) % max_direct)


class Tank(UObject):
    def __init__(self):
        self.__propertice: dict = {}

    def get_property(self, key: str) -> Vector:
        return self.__propertice.get(key, None).vector

    def set_property(self, key: str, value: Vector) -> None:
        self.__propertice[key] = value
