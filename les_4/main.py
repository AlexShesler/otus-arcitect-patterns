from abc import ABCMeta, abstractmethod
from threading import Thread
from queue import Queue
from typing import Optional
from les_3 import main


class IQueueCommand(metaclass=ABCMeta):
    @abstractmethod
    def put_command(self, command: main.Command) -> None:
        pass

    @abstractmethod
    def get_command(self) -> main.Command:
        pass


class IActor(metaclass=ABCMeta):
    @abstractmethod
    def start_queue(self, queue: IQueueCommand) -> None:
        pass

    @abstractmethod
    def stop_queue(self, queue: IQueueCommand) -> None:
        pass


class HardStopLoop(main.Command):
    def execute(self) -> None:
        raise StopIteration('Loop stopped hard')


class SoftStopLoop(main.Command, Queue, IQueueCommand):
    def put_command(self, command: Optional[main.Command]) -> None:
        self.put(command, block=True)

    def execute(self) -> None:
        self.put_command(None)


class ThreadedQueue(Queue, IQueueCommand):
    def put_command(self, command: main.Command) -> None:
        self.put(command, block=True)

    def get_command(self) -> main.Command:
        command = self.get(block=True)
        return command


class ThreadedActor(Thread, IActor):
    def __init__(self):
        self.queue: Optional[IQueueCommand] = None
        Thread.__init__(self)

    def start_queue(self, queue: IQueueCommand) -> None:
        self.queue = queue
        Thread.start(self)

    def command_loop(self) -> None:
        while 1:
            command: main.Command = self.queue.get_command()
            try:
                command.execute()
            except StopIteration:
                break
            except Exception as ex:
                print(f'Exception:\n{ex}')

    def run(self) -> None:
        print(f'Thread {self.name} started')
        self.command_loop()
        return

    def stop_queue(self, queue: IQueueCommand) -> None:
        pass
