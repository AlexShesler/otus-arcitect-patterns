import unittest
from les_3 import main
import error


class Test(unittest.TestCase):
    def setUp(self) -> None:
        self.tank = main.Tank()

    def test_move_tank(self):
        self.tank.set_property("position", main.Vector([12, 5]))
        self.tank.set_property("velocity", main.Vector([-7, 3]))
        self.move = main.Move(main.MovableAdapter(self.tank))
        self.move.execute()
        self.assertEqual(self.tank.get_property("position"), [5, 8])

    def test_read_position(self):
        self.move = main.Move(main.MovableAdapter(self.tank))
        with self.assertRaises(error.NotReadPropertice):
            self.move.execute()

    def test_read_velocity(self):
        self.tank.set_property("position", main.Vector([12, 5]))
        self.move = main.Move(main.MovableAdapter(self.tank))
        with self.assertRaises(error.NotReadPropertice):
            self.move.execute()

    def test_move_error(self):
        self.tank.set_property("position", main.Vector([12, 5]))
        self.tank.set_property("velocity", main.Vector([-13, 3]))
        self.move = main.Move(main.MovableAdapter(self.tank))
        with self.assertRaises(error.OutOfRange):
            self.move.execute()

    def test_rotate_tank(self):
        self.tank.set_property("direction", main.Vector([0]))
        self.tank.set_property("angular_velocity", main.Vector([14]))
        self.rotate = main.Rotate(main.RotableAdapter(self.tank))
        self.rotate.execute()
        self.assertEqual(self.tank.get_property("direction"), [2])

    def test_read_derection(self):
        self.rotate = main.Rotate(main.RotableAdapter(self.tank))
        with self.assertRaises(error.NotReadPropertice):
            self.rotate.execute()

    def test_read_angular_velocity(self):
        self.tank.set_property("direction", main.Vector([0]))
        self.rotate = main.Rotate(main.RotableAdapter(self.tank))
        with self.assertRaises(error.NotReadPropertice):
            self.rotate.execute()


if __name__ == "__main__":
    unittest.main()
