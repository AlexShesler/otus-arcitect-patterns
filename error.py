class CustomException(Exception):
    def __init__(self, error_text: str):
        print(error_text)


class NotReadPropertice(CustomException):
    pass


class OutOfRange(CustomException):
    pass


class VectorMismatch(CustomException):
    pass
